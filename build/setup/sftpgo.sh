#!/bin/bash
set -e
source /bd_build/buildconfig
set -x

mkdir -p /var/shwoncast/sftpgo/persist /var/shwoncast/sftpgo/backups

cp /bd_build/sftpgo/sftpgo.json /var/shwoncast/sftpgo/sftpgo.json

touch /var/shwoncast/sftpgo/sftpgo.db
chown -R shwoncast:shwoncast /var/shwoncast/sftpgo