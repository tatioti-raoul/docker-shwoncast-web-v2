#!/bin/bash
set -e
source /bd_build/buildconfig
set -x

$minimal_apt_get_install sudo

# Workaround for sudo errors in containers, see: https://github.com/sudo-project/sudo/issues/42
echo "Set disable_coredump false" >> /etc/sudo.conf

adduser --home /var/shwoncast --disabled-password --gecos "" shwoncast

usermod -aG docker_env shwoncast
usermod -aG www-data shwoncast

mkdir -p /var/shwoncast/www /var/shwoncast/backups /var/shwoncast/www_tmp /var/shwoncast/geoip /var/shwoncast/dbip

chown -R shwoncast:shwoncast /var/shwoncast
chmod -R 777 /var/shwoncast/www_tmp

echo 'shwoncast ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers