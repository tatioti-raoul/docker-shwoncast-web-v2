#!/bin/bash

# Copy the php.ini template to its destination.
dockerize -template "/etc/php/7.4/fpm/05-shwoncast.ini.tmpl:/etc/php/7.4/fpm/conf.d/05-shwoncast.ini" -template "/etc/php/7.4/fpm/www.conf.tmpl:/etc/php/7.4/fpm/pool.d/www.conf" cp /etc/php/7.4/fpm/conf.d/05-shwoncast.ini /etc/php/7.4/cli/conf.d/05-shwoncast.ini
#php /var/shwoncast/www/src/Messaging/Server.php start -d
