#!/bin/bash

if [[ ! -f /var/shwoncast/sftpgo/persist/id_rsa ]]; then
    ssh-keygen -t rsa -b 4096 -f /var/shwoncast/sftpgo/persist/id_rsa -q -N ""
fi

chown -R shwoncast:shwoncast /var/shwoncast/sftpgo/persist